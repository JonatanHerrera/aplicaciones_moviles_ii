package com.example.noticiasupt.ViewModel;

public class Peticion_Notificacion {

    public String estado;
    public int usuarioId;
    public String titulo;
    public String desc;

    public Peticion_Notificacion(int usuarioId, String titulo, String desc) {
        this.usuarioId = usuarioId;
        this.titulo = titulo;
        this.desc = desc;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
