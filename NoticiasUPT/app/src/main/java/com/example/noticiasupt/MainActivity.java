package com.example.noticiasupt;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.noticiasupt.Api.Api;
import com.example.noticiasupt.Servicios.ServicioPeticion;
import com.example.noticiasupt.ViewModel.Peticion_Login;
import com.example.noticiasupt.ViewModel.RegistrarUsuario;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private Button Crear, login;

    private EditText us, con;

    private String APITOKEN;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Crear=(Button)findViewById(R.id.btnRegistro);
        login=(Button)findViewById(R.id.btnIngresar);

        us=(EditText)findViewById(R.id.txtUs);
        con=(EditText)findViewById(R.id.txtContra);

        SharedPreferences preferences = getSharedPreferences("credenciales",Context.MODE_PRIVATE);
        String token = preferences.getString("TOKEN", "");
        if(token !="")
        {
            Toast.makeText(MainActivity.this,"Bienvenido de nuevo",Toast.LENGTH_LONG).show();
            startActivity(new Intent(MainActivity.this, Menu_principal.class));
        }

        Crear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, Registro.class));
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(us.getText().toString().isEmpty() || con.getText().toString().isEmpty())
                {
                    Toast.makeText(MainActivity.this,"Campos vacios",Toast.LENGTH_LONG).show();
                }
                else
                {
                    ServicioPeticion servicioPeticion = Api.getApi(MainActivity.this).create(ServicioPeticion.class);
                    Call<Peticion_Login> LoginCall = servicioPeticion.getLogin(us.getText().toString(), con.getText().toString());
                    LoginCall.enqueue(new Callback<Peticion_Login>() {
                        @Override
                        public void onResponse(Call<Peticion_Login> call, Response<Peticion_Login> response) {
                            Peticion_Login peticion = response.body();
                            if(response.body()==null)
                            {
                                Toast.makeText(MainActivity.this,"Ha ocurrido un error, intentelo mas tarde",Toast.LENGTH_LONG).show();
                                return;
                            }
                            if(peticion.estado=="true")
                            {
                                APITOKEN = peticion.token;
                                GuardarReferencias();
                                startActivity(new Intent(MainActivity.this, Menu_principal.class));
                                Toast.makeText(MainActivity.this,"Login exitoso",Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<Peticion_Login> call, Throwable t) {
                            Toast.makeText(MainActivity.this,"Error",Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });
    }

    public void GuardarReferencias()
    {
        SharedPreferences preferences = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String token = APITOKEN;
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("TOKEN", token);
        editor.commit();
    }
}
