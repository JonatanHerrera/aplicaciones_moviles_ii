package com.example.noticiasupt;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.noticiasupt.Api.Api;
import com.example.noticiasupt.Servicios.ServicioPeticion;
import com.example.noticiasupt.ViewModel.Peticion_Noticias;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Menu1 extends AppCompatActivity {

    private Button prueba;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu1);

        prueba=(Button)findViewById(R.id.btnPrueba);

        prueba.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ServicioPeticion servicioPeticion = Api.getApi(Menu1.this).create(ServicioPeticion.class);
                Call<Peticion_Noticias> peticion_noticiasCall = servicioPeticion.getNoticias();
                peticion_noticiasCall.enqueue(new Callback<Peticion_Noticias>() {
                    @Override
                    public void onResponse(Call<Peticion_Noticias> call, Response<Peticion_Noticias> response) {
                        if(response.isSuccessful())
                        {
                            if(response.body().estado)
                            {
                                List<Peticion_Noticias.Datos> detalles = response.body().detalle;
                                for(Peticion_Noticias.Datos datos:detalles)
                                {
                                    Log.i("api",datos.getDescripcion());
                                    Toast.makeText(Menu1.this,datos.getDescripcion(),Toast.LENGTH_LONG).show();
                                }
                            }
                        }
                        else
                        {
                            Toast.makeText(Menu1.this,"Datos incorrectos",Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Peticion_Noticias> call, Throwable t) {
                        Toast.makeText(Menu1.this,"Error desconocido",Toast.LENGTH_LONG).show();
                    }
                });
            }
        });

    }

    public void Noticias()
    {

    }
}
