package com.example.noticiasupt.Servicios;

import com.example.noticiasupt.ViewModel.Peticion_Login;
import com.example.noticiasupt.ViewModel.Peticion_Noticias;
import com.example.noticiasupt.ViewModel.Peticion_Notificacion;
import com.example.noticiasupt.ViewModel.RegistrarUsuario;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ServicioPeticion {
    @FormUrlEncoded
    @POST("api/crearUsuario")
    Call<RegistrarUsuario> registrarUsario(@Field("username")String correo, @Field("password")String password);

    @FormUrlEncoded
    @POST("api/login")
    Call<Peticion_Login> getLogin(@Field("username")String correo, @Field("password")String password);

    @GET("api/todasNot")
    Call<Peticion_Noticias> getNoticias();

    @POST("api/crearNotUsuario")
    Call<Peticion_Notificacion> notificacion(@Field("usuarioId")int usuarioId, @Field("titulo")String Titulo, @Field("descripcion")String desc );

}
