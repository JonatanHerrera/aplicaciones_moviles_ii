package com.example.noticiasupt;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.noticiasupt.Api.Api;
import com.example.noticiasupt.Servicios.ServicioPeticion;
import com.example.noticiasupt.ViewModel.Peticion_Noticias;
import com.example.noticiasupt.ViewModel.Peticion_Notificacion;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Menu_principal extends AppCompatActivity {

    private Button prueba,not;

    private EditText id,tit,des;
    ServicioPeticion peticion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_principal);

        prueba = (Button)findViewById(R.id.btnPrueba);
        not=(Button)findViewById(R.id.btnN);

        id=(EditText)findViewById(R.id.txtId1);
        tit=(EditText)findViewById(R.id.txtTitulo);
        des=(EditText)findViewById(R.id.txtDes);


        prueba.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                peticion = Api.getApi(Menu_principal.this).create(ServicioPeticion.class);
                Noticias();
            }
        });
        not.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(id.getText().toString().isEmpty() || tit.getText().toString().isEmpty() || tit.getText().toString().isEmpty())
                {
                    Toast.makeText(Menu_principal.this,"Falta llenar campos",Toast.LENGTH_LONG).show();
                }
                else
                {
                    ServicioPeticion servicioPeticion = Api.getApi(Menu_principal.this).create(ServicioPeticion.class);
                    Call<Peticion_Notificacion> peticion_notificacionCall = servicioPeticion.notificacion(Integer.parseInt(id.getText().toString()), tit.getText().toString(), des.getText().toString());
                    peticion_notificacionCall.enqueue(new Callback<Peticion_Notificacion>() {
                        @Override
                        public void onResponse(Call<Peticion_Notificacion> call, Response<Peticion_Notificacion> response) {
                            Peticion_Notificacion peticion = response.body();
                            if(response.body() == null)
                            {
                                Toast.makeText(Menu_principal.this,"Ha ocurrido un error, intentelo mas tarde",Toast.LENGTH_LONG).show();
                                return;
                            }
                            if(peticion.estado=="true")
                            {
                                Toast.makeText(Menu_principal.this,"Notificacion exitosa",Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<Peticion_Notificacion> call, Throwable t) {
                            Toast.makeText(Menu_principal.this,"Error",Toast.LENGTH_LONG).show();
                        }
                    });
                }

            }
        });
    }

    public void Noticias()
    {
        peticion.getNoticias().enqueue(new Callback<Peticion_Noticias>() {
            @Override
            public void onResponse(Call<Peticion_Noticias> call, Response<Peticion_Noticias> response) {
                if(response.isSuccessful())
                {
                    if(response.body().estado)
                    {
                        List<Peticion_Noticias.Datos> detalles = response.body().detalle;
                        for(Peticion_Noticias.Datos datos: detalles)
                        {
                            Toast.makeText(Menu_principal.this,datos.getDescripcion(),Toast.LENGTH_LONG).show();
                        }
                    }
                }
                else
                {
                    Toast.makeText(Menu_principal.this,"Surgio un error",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Peticion_Noticias> call, Throwable t) {
                Toast.makeText(Menu_principal.this,"Surgio un error",Toast.LENGTH_LONG).show();
            }
        });
    }
}
