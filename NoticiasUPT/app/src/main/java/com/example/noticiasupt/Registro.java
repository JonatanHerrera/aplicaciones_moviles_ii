package com.example.noticiasupt;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.noticiasupt.Api.Api;
import com.example.noticiasupt.Servicios.ServicioPeticion;
import com.example.noticiasupt.ViewModel.RegistrarUsuario;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Registro extends AppCompatActivity {

    private Button Crear;
    private EditText usuario;
    private EditText contra,contra2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        Crear = (Button)findViewById(R.id.btnCrear);
        usuario = (EditText)findViewById(R.id.txtUs);
        contra = (EditText)findViewById(R.id.txtContra);
        contra2 = (EditText)findViewById(R.id.txtContra2);

        Crear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String c1,c2;
                c1=contra.getText().toString();
                c2=contra2.getText().toString();

                if(usuario.getText().toString().isEmpty() || contra.getText().toString().isEmpty() || contra2.getText().toString().isEmpty())
                {
                    Toast.makeText(Registro.this, "Campos vacios",Toast.LENGTH_LONG).show();
                }
                else {
                    if(c1.equals(c2))
                    {
                        ServicioPeticion servicioPeticion = Api.getApi(Registro.this).create(ServicioPeticion.class);
                        Call<RegistrarUsuario> registrarUsuarioCall = servicioPeticion.registrarUsario(usuario.getText().toString(), contra.getText().toString());
                        registrarUsuarioCall.enqueue(new Callback<RegistrarUsuario>() {
                            @Override
                            public void onResponse(Call<RegistrarUsuario> call, Response<RegistrarUsuario> response) {
                                RegistrarUsuario peticion = response.body();
                                if(response.body()==null)
                                {
                                    Toast.makeText(Registro.this,"Ha ocurrido un error, intentelo mas tarde",Toast.LENGTH_LONG).show();
                                    return;
                                }
                                if(peticion.estado=="true")
                                {
                                    startActivity(new Intent(Registro.this, MainActivity.class));
                                    Toast.makeText(Registro.this,"Registro exitoso",Toast.LENGTH_LONG).show();
                                }
                                else
                                {
                                    Toast.makeText(Registro.this,peticion.detalle,Toast.LENGTH_LONG).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<RegistrarUsuario> call, Throwable t) {
                                Toast.makeText(Registro.this,"Error",Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                    else
                    {
                        Toast.makeText(Registro.this,"Las contarseñas no son iguales",Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }
}
